package net.jxvtc;

import lombok.extern.slf4j.Slf4j;
import net.jxvtc.feign.WeatherFeignClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;


@Slf4j
@SpringBootTest
public class WeatherTest {

    @Autowired
    WeatherFeignClient weatherFeignClient;

    @Test
    public void testWeather(){
        String result = weatherFeignClient.getCondition("2182",
                "50b53ff8dd7d9fa320d3d3ca32cf8ed1",
                "APPCODE 93b7e19861a24c519a7548b17dc16d75");
        log.info(result);
    }
}
