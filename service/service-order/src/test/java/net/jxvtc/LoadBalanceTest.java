package net.jxvtc;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;

@Slf4j
@SpringBootTest
public class LoadBalanceTest {
    @Autowired
    LoadBalancerClient loadBalancerClient;

    @Test
    public void testLoadBalance() {
        ServiceInstance choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
        choose = loadBalancerClient.choose("service-product");
        log.info("http://"+choose.getHost()+":"+choose.getPort());
    }
}
