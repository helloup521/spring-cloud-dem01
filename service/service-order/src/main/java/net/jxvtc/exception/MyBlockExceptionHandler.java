package net.jxvtc.exception;

import com.alibaba.csp.sentinel.adapter.spring.webmvc_v6x.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.jxvtc.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;

@Component
public class MyBlockExceptionHandler implements BlockExceptionHandler {


   private ObjectMapper objectMapper=new ObjectMapper();

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, String resourceName, BlockException ex) throws Exception {

        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();

        R r=R.error(500,"超出了每秒1次响应,错误原因："+ex.getMessage());
        String result = objectMapper.writeValueAsString(r);
        out.println(result);
        out.flush();
        out.close();
    }
}
