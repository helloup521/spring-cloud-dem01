package net.jxvtc.controller;

import net.jxvtc.bean.Order;
import net.jxvtc.property.OrderProperty;
import net.jxvtc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//注意：没有@RefreshScope注解时，Nacaos中配置发生变化后，微服务访问到的还是改变前的配置，只有加上该注解才可用

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
   OrderProperty orderProperty;

    @GetMapping("/create")
    public Order createOrder(@RequestParam("userId") Long userId,
                             @RequestParam("productId") Long productId) {
        Order order = orderService.createOrder(userId, productId);
        return order;
    }

    @GetMapping("/config")
    public String getConfig() {
        return String.format("order.timeout=%s,order.auto-confirm=%s,db-url=%s", orderProperty.getTimeout(),
                orderProperty.getAutoConfirm(),orderProperty.getDbUrl());
    }

}
