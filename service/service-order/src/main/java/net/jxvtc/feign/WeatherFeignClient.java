package net.jxvtc.feign;

import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

//@FeignClient(value="service-client",url = "https://aliv18.data.moji.com")
@FeignClient(value = "weather-client", url = "http://aliv18.data.moji.com")
public interface WeatherFeignClient {

    @PostMapping("/whapi/json/alicityweather/condition")
    public String getCondition(@RequestParam("cityId") String cityId,
                               @RequestParam("token") String token,
                               @RequestHeader("Authorization") String auth);
}
