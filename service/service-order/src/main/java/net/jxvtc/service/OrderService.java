package net.jxvtc.service;

import net.jxvtc.bean.Order;

public interface OrderService {

    Order createOrder(Long userId, Long productId);
}
