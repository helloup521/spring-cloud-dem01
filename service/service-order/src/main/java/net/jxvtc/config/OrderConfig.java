package net.jxvtc.config;

import feign.Logger;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class OrderConfig {

    @LoadBalanced
    @Bean
    public RestTemplate getRestTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }

   @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }
}

//思考题：注册中心宕机了，远程调用还能成功吗？
//如果第一次访问，则不能成功。
//如果不是第一次访问，由于缓存机制，可以调用成功