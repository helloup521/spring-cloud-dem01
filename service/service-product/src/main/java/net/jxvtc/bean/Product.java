package net.jxvtc.bean;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    /**
     * 商品id
     */
    private Long id;
    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 购买数量
     */
    private int num;
}
