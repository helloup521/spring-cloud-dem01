package net.jxvtc.controller;

import lombok.extern.slf4j.Slf4j;
import net.jxvtc.bean.Product;
import net.jxvtc.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/product/{id}")
    public Product getProduct(@PathVariable("id") Long productId){
        Product product=productService.getProductById(productId);
        log.info("hello");
        return product;
    }
}
