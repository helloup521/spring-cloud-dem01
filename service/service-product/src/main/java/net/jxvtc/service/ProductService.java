package net.jxvtc.service;

import net.jxvtc.bean.Product;

public interface ProductService {
    Product getProductById(Long productId);
}
