package net.jxvtc.service.impl;
import java.math.BigDecimal;

import net.jxvtc.bean.Product;
import net.jxvtc.service.ProductService;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Override
    public Product getProductById(Long productId) {
        Product product=new Product();
        product.setId(productId);
        product.setPrice(new BigDecimal("99.5"));
        product.setProductName("充电宝"+productId);
        product.setNum(2);

        return product;
    }
}
