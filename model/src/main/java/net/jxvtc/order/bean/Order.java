package net.jxvtc.order.bean;

import lombok.Data;
import net.jxvtc.product.bean.Product;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Order {
    private Long id;
    private BigDecimal totalAmount;
    private Long userId;
    private String nickName;
    private String address;
    private List<Product> productList;
}
