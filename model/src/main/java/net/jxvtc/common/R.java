package net.jxvtc.common;

import lombok.Data;

@Data
public class R {
    private Integer code;
    private String msg;
    private Object data;

    public static R ok(){
       R instance =new R();
       instance.setCode(200);
       return instance;
    }

    public static R ok(Integer code ,String msg ,Object data){
        R r=new R();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

    public static R error(Integer code,String msg){
        R r=new R();

        r.setMsg(msg);
        r.setCode(code);
        return r;
    }

    public static R error(){
        R instance=new R();
        instance.setCode(500);
        return instance;
    }
}
